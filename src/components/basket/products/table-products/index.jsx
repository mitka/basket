import React from 'react'
import PropTypes from 'prop-types'

import { THeader } from '../t-header'

import style from './style.css'

export const TableProducts = ({
    setValueForSortColl,
    sort,
    direction,
    children,
}) => (
    <table className={style.table}>
        <thead className={style.tableHead}>
            <THeader
                setValueForSortColl={setValueForSortColl}
                sort={sort}
                direction={direction}
            />
        </thead>
        <tbody>{children}</tbody>
    </table>
)

TableProducts.propTypes = {
    setValueForSortColl: PropTypes.func.isRequired,
    sort: PropTypes.string,
    direction: PropTypes.bool,
    children: PropTypes.node.isRequired,
}

TableProducts.defaultProps = {
    sort: void 0,
    direction: true,
}
