import React from 'react'
import PropTypes from 'prop-types'
import { actions as actionsStore, selectors } from 'store' // eslint-disable-line
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import style from './style.css'


const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actionsStore.basket, dispatch),
})

const mapStateToProps = state => ({
    sum: selectors.getSumBasket(state),
})

export const FooterProductsDumb = ({ actions, sum }) => (
    <tr className={style.rowFooter}>
        <td colSpan={2} />
        <td className={style.colPrice}>{sum.toFixed(2)} $</td>
        <td />
        <td>
            <button onClick={actions.buy} className={style.buy}>Buy</button>
        </td>
    </tr>
)

FooterProductsDumb.propTypes = {
    sum: PropTypes.number,
    actions: PropTypes.shape({
        buy: PropTypes.func,
    }).isRequired,
}

FooterProductsDumb.defaultProps = {
    sum: 0,
}

export const FooterProducts = connect(mapStateToProps, mapDispatchToProps)(FooterProductsDumb)
