import React from 'react'
import PropTypes from 'prop-types'
import { actions as actionsStore, selectors } from 'store' // eslint-disable-line

import { TableProducts } from './table-products'
import { ListProducts } from './list-products'
import { FooterProducts } from './footer-products'

export class ProductsContainer extends React.PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            sort: void 0,
            direction: true,
        }
    }

    setValueForSortColl(sort) {
        if (this.state.sort === sort) {
            return this.setState({ sort, direction: !this.state.direction })
        }
        return this.setState({ sort, direction: true })
    }

    getSortFunction(a, b) {
        const { sort } = this.state
        const direction = this.state.direction ? 1 : -1
        const condition = a[sort] > b[sort]
        if (condition) {
            return direction
        }
        if (!condition) {
            return -direction
        }
        return 0
    }
    sortProducts(items) {
        if (this.state.sort) {
            return [].concat(items.sort(this.getSortFunction.bind(this))) // return new array
        }
        return items
    }

    render() {
        const { items } = this.props
        return (
            <TableProducts
                setValueForSortColl={this.setValueForSortColl.bind(this)}
                sort={this.state.sort}
                direction={this.state.direction}
            >
                <ListProducts items={this.sortProducts(items)} />
                <FooterProducts />
            </TableProducts>
        )
    }
}

ProductsContainer.propTypes = {
    items: PropTypes.array, // eslint-disable-line
}

ProductsContainer.defaultProps = {
    items: [],
}
