import React from 'react'
import { selectors } from 'store' // eslint-disable-line
import { connect } from 'react-redux'

import { ProductsContainer } from './products'

import style from './style.css'

const mapStateToProps = state => ({
    products: selectors.getBasketProducts(state),
})

export const Basket = connect(mapStateToProps)(({ products }) => (
    <div className={style.blockBasket}>
        <h1>Basket</h1>
        {!products.length && 'Select a product'}
        {!!products.length && <ProductsContainer items={products} />}
    </div>
))
