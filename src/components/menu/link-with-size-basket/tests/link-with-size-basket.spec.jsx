import React from 'react'
import { HashRouter } from 'react-router-dom'
import renderer from 'react-test-renderer'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import Immutable from 'immutable'

import { LinkWithSizeBasket } from '../'

const FIVE = 5

describe('Component showed size basket:', () => {
    const state = {
        basket: new Immutable.List(new Array(FIVE)),
    }

    const store = configureStore()(state)

    const components = (
        <Provider store={store}>
            <HashRouter>
                <LinkWithSizeBasket to="/">Basket</LinkWithSizeBasket>
            </HashRouter>
        </Provider>)

    const wrapper = renderer.create(components)

    it('export LinkWithSizeBasket is correct', () => {
        expect(LinkWithSizeBasket).toBeDefined()
    })

    it('show size', () => {
        const { children } = wrapper.toJSON()
        expect(children[0]).toBe('Basket')
        expect(+children[2]).toBe(FIVE)
    })
})
