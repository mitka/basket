import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { selectors } from 'store' // eslint-disable-line

const mapStateToProps = state => ({
    sizeBasket: selectors.getSizeBasket(state),
})

export const LinkWithSizeBasket = connect(mapStateToProps)(({
    children,
    dispatch, // omit
    sizeBasket,
    ...etc
}) => (<Link {...etc}>{children} ({sizeBasket})</Link>))
