import React from 'react'
import { Link } from 'react-router-dom'

import { LinkWithSizeBasket } from './link-with-size-basket'

import style from './style.css'

export const Menu = () => (
    <div className={style.menu}>
        <nav className={style.menuNavigation}>
            <Link className={style.menuNavigationLink} to="/" href="/">Products</Link>
            <LinkWithSizeBasket
                className={style.menuNavigationLink}
                to="/basket"
                href="/basket"
            >
                Basket
            </LinkWithSizeBasket>
        </nav>
    </div>
)
