import React from 'react'
import PropTypes from 'prop-types'
import { Route, HashRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { actions } from 'store' // eslint-disable-line

import { ListProducts } from '../list-products'
import { Basket } from '../basket'
import { Menu } from '../menu'

import style from './style.css'

class AppDumb extends React.Component {
    componentDidMount() {
        this.props.actions.fetchProducts()
    }

    render() {
        return (
            <HashRouter>
                <React.Fragment>
                    <Menu />
                    <main className={style.main}>
                        <Route path="/" exact component={ListProducts} />
                        <Route path="/basket" component={Basket} />
                    </main>
                </React.Fragment>
            </HashRouter>
        )
    }
}

AppDumb.propTypes = {
    actions: PropTypes.shape({
        fetchProducts: PropTypes.func,
    }),
}

AppDumb.defaultProps = {
    actions: {
        fetchProducts: () => {},
    },
}


const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(actions.products, dispatch),
})

export const App = connect(null, mapDispatchToProps)(AppDumb)
