import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { selectors, actions } from 'store' //eslint-disable-line

import style from './style.css'

const mapStateToProps = state => ({
    products: selectors.getProducts(state),
})

const mapDispatchToProps = dispatch => bindActionCreators(actions.basket, dispatch)

export const ListProducts = connect(
    mapStateToProps,
    mapDispatchToProps,
)(({ products, addCart }) => (
    <div className={style.products}>
        {
            products.map(product => (
                <div className={style.productsItem} key={product.id}>
                    <div className={style.productsItemImg} style={{ background: `url(${product.img})` }} />
                    <p className={style.productsItemCaption}>{product.caption}</p>
                    <p className={style.productsItemPrice}>{product.price} $</p>
                    <button
                        onClick={() => addCart(product)}
                        className={style.productsItemButton}
                    >
                        Add to cart
                    </button>
                </div>
            ))
        }
    </div>
))
