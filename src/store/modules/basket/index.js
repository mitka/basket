import { createAction, createReducer } from 'redux-act'
import Immutable from 'immutable'

import { selectors } from 'store' // eslint-disable-line

export const addCart = createAction('Add product in cart')
export const deleteCart = createAction('Delete product in cart')
export const changeCountCart = createAction('Change count product in basket')

export const buy = () => (dispatch, getState) =>
    fetch('/', {
        method: 'POST',
        body: JSON.stringify(selectors.getBasketProducts(getState())),
    })

const BASKET = 'basket'

const getInitState = () => {
    try {
        return new Immutable.Map(JSON.parse(localStorage.getItem(BASKET)))
    } catch (e) {
        return new Immutable.Map()
    }
}

const setLocalStorage = (state) => {
    localStorage.setItem(BASKET, JSON.stringify(state.toJS()))
    return state
}

export const reducers = createReducer({
    [addCart]: (state, products) => setLocalStorage(state.set(`${products.id}`, Object.assign({}, products, { count: 1 }))),
    [deleteCart]: (state, id) => setLocalStorage(state.delete(`${id}`)),
    [changeCountCart]: (state, { id, count }) => setLocalStorage(state.update(`${id}`, item => Object.assign({}, item, { count: +count }))),
}, getInitState())
