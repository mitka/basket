import {
    addProducts,
    fetchProducts,
    reducers as reducersProduct,
} from './products'
import {
    addCart,
    changeCountCart,
    deleteCart,
    buy,
    reducers as reducersBasket,
} from './basket'

export const reducers = {
    products: reducersProduct,
    basket: reducersBasket,
}

export const actions = {
    products: {
        addProducts,
        fetchProducts,
    },
    basket: {
        buy,
        addCart,
        changeCountCart,
        deleteCart,
    },
}
