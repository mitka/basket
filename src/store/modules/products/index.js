import { createAction, createReducer } from 'redux-act'
import Immutable from 'immutable'

export const addProducts = createAction('Add products')
export const fetchProducts = () => async (dispatch) => {
    const response = await fetch('./public/api/list-products.json')
    const products = await response.json()
    dispatch(addProducts(products))
}

export const reducers = createReducer({
    [addProducts]: (state, products) => new Immutable.List(products),
}, new Immutable.List())
