import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'

import * as selectors from './selectors'
import { reducers, actions } from './modules'

const getDevTools = () => {
    /* eslint-disable no-underscore-dangle */
    if (window.__REDUX_DEVTOOLS_EXTENSION__) {
        return window.__REDUX_DEVTOOLS_EXTENSION__({
            name: 'basket',
        })
    }
    /* eslint-enable */

    return void 0
}

export function storeCreator(additionalReducerFn) {
    const reducer = combineReducers(reducers)

    if (additionalReducerFn) {
        return compose(
            applyMiddleware(thunkMiddleware),
            additionalReducerFn,
        )(createStore)(reducer, {})
    }

    return applyMiddleware(thunkMiddleware)(createStore)(reducer, {})
}

export default compose(storeCreator, getDevTools)()

export {
    actions,
    selectors,
}
