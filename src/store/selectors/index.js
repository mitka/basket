import { createSelector } from 'reselect'

const productSlice = state => state.products
const basketSlice = state => state.basket

export const getProducts = createSelector(productSlice, items => items.toJS())

export const getBasket = createSelector(basketSlice, items => items)

export const getBasketProducts = createSelector(
    getBasket,
    items => items.toList().toJS(),
)

export const getSizeBasket = createSelector(getBasket, items => items.size)

export const getSumBasket = createSelector(getBasket, items =>
    items.reduce((result, item) =>
        result + (item.count * +item.price), 0))
