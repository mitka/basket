const path = require('path')

const rollupPreprocessor = require('./rollup.config')

const tests = ['./src/**/tests/*.spec.+(js|jsx)']

const files = tests.map(test => (
    { pattern: test, watched: false }
))

const preprocessors = tests.reduce((result, test) => Object.assign(result, { [test]: ['rollup'] }), {})

module.exports = (config) => {
    config.set({

        basePath: '',

        frameworks: ['jasmine'],

        port: 9876,
        browserNoActivityTimeout: 60000,
        browsers: ['Chrome'],
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        singleRun: false,
        autoWatchBatchDelay: 300,

        files,

        preprocessors,

        rollupPreprocessor,

        plugins: [
            'karma-rollup-preprocessor',
            'karma-jasmine',
            'karma-chrome-launcher',
            'karma-coverage',
        ],

        coverageReporter: {
            dir: path.resolve(__dirname, 'reports/coverage'),
            subdir: '.',
            reporters: [
                {
                    type: 'html',
                    dir: 'reports/coverage/html',
                },
            ],
        },

        reporters: ['progress', 'coverage'],
    })
}
