const babel = require('rollup-plugin-babel')
const commonjs = require('rollup-plugin-commonjs')
const resolve = require('rollup-plugin-node-resolve')
const serve = require('rollup-plugin-serve')
const alias = require('rollup-plugin-alias')
const replace = require('rollup-plugin-replace')
const postcss = require('rollup-plugin-postcss')
const postcssModules = require('postcss-modules')
const nested = require('postcss-nested')

const cssExportMap = {}

module.exports = {
    input: 'src/main.jsx',
    output: {
        file: 'dist/index.js',
        format: 'iife',
        sourcemap: 'inline',
    },
    plugins: [
        serve({
            open: true,
            verbose: false,
            port: 3333,
        }),
        alias({
            store: 'src/store/index.js',
        }),
        resolve({
            jsnext: true,
            main: true,
            browser: true,
            extensions: ['.jsx', '.js'],
        }),
        commonjs({
            namedExports: {
                'node_modules/react/index.js': ['Children', 'Component', 'createElement'],
            },
        }),
        replace({
            'process.env.NODE_ENV': JSON.stringify('development'),
        }),
        postcss({
            plugins: [
                nested(),
                postcssModules({
                    generateScopedName: '[name]__[local]___[hash:base64:5]',
                    getJSON: (id, exportTokens) => {
                        cssExportMap[id] = exportTokens
                    },
                }),
            ],
            getExport: id => cssExportMap[id],
        }),
        babel({
            compact: false,
            exclude: 'node_modules/**',
        }),
    ],
}
