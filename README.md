# npm run dev
Start dev server on 3333 port [localhost:3333](http://localhost:3333)

## npm run build
Build project in development mode to dist path

## npm run start
Start static server for build project

## npm run test
Run jasmine tests

## npm run test:watch
Run tests with watch mode


Live preview: [https://shrouded-wave-31360.herokuapp.com/#/](https://shrouded-wave-31360.herokuapp.com/#/)